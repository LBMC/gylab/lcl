## Claire Burny - Genotypes extraction
Data downloaded on 13th February 2017: ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/
```sh
wget ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/name_file_to_download (-b optional to be in background)
```

##### .VCF file
One file is a .VCF file per chromosome containing phased genotype data from phase 3 individuals of 1000Genomes Project for chr1-22 and chr X and Y from Illumina platform sequence >=70bp. One line is one variant and one column is one individual. One dbSNP ID aims at uniquely identifying each SNP accross dataset (rs+number, not always available, lastest rs numbers are not available for chrX, Y and the 3M patched up chunk on chr12 yet). It contains informations on reference allele on hg19 (GRCh37), on ancestral allele (AA), on AC alternate alleles on a total number of AN alleles (MULTI_ALLELIC flag indicate whether the site is multi allelic) where VT indicates the type of variant. AFR_AF is the allele frequency in the AFR populations calculated from AC and AN.
```
Y	2655180	rs11575897	G	A	100	PASS	AA=G;AC=22;AN=1233;DP=84761;NS=1233;VT=SNP;EX_TARGET;AMR_AF=0.0000;AF=0.0178;AFR_AF=0.0000;EUR_AF=0.0000;SAS_AF=0.0000;EAS_AF=0.0902
1	564621	rs10458597	C	T	.	.	.	GT	0/0	0/0	0/0	0/0	0/0	0/0	0/0	0/0
1	564773	.	C	T	.	.	.	GT	0/1	0/0	0/0	0/0	0/0	0/0	0/0	0/0
1	721290	rs12565286	G	C	.	.	.	GT	0/0	0/0	0/0	0/0	0/0	0/0	0/0	0/0
```
Here is some basic information from README_phase3_callset_20150220:

```
The total number of sites in the autosomes file is 81271745
The breakdown of these sites is:
number of SNPs:	78136341
number of indels:	3135424
number of others:	58671
number of multiallelic sites:	416023
number of multiallelic SNP sites:	259370
The stats for chrX
number of samples:	2504
number of records:	3468087
number of SNPs:	3246232
number of MNPs:	0
number of indels:	227112
number of others:	2040
number of multiallelic sites:	30994
number of multiallelic SNP sites:	1505
```

##### .ped file
Informations on sexe and kinship between individuals(2,504 individuals from 26 pop categorized by continent, EAS, SAS, AFR, EUR, AMR) is available in integrated_call_samples_v2.20130502.ALL.ped. One relationship remains in our individuals: NA18913 is the child of NA19238. 

##### subsetting on individuals
 I subsetted the VCF (by columns) to get only the individuals of interest for which Gerard Triqueneaux made the fluo experiments:
```
ALL.chr4_selected.vcf.gz
ALL.chr5_selected.vcf.gz
ALL.chr6_selected.vcf.gz
ALL.chr7_selected.vcf.gz
ALL.chr8_selected.vcf.gz
ALL.chr9_selected.vcf.gz
ALL.chr10_selected.vcf.gz
ALL.chr11_selected.vcf.gz
ALL.chr13_selected.vcf.gz
ALL.chr14_selected.vcf.gz
ALL.chr15_selected.vcf.gz
ALL.chr16_selected.vcf.gz
ALL.chr17_selected.vcf.gz
ALL.chr18_selected.vcf.gz
ALL.chr19_selected.vcf.gz
ALL.chr20_selected.vcf.gz
ALL.chr21_selected.vcf.gz
ALL.chr22_selected.vcf.gz
```
For the moment, I did not subset chrX, Y and MT (some problems, no data available for some individuals in these chromosomes, to see later how to solve if needed to work on these chromosomes).
You first need to index your samples with tabix:
```
tabix -p vcf input_indexed.vcf.gz
```
You can then use vcf_tools to subset:
```
perl vcf-subset -c NA19098,NA19099,NA19107,NA19108,NA19141,NA19204,NA19238,NA19239,NA18486,NA18488,NA18489,NA18498,NA18499,NA18501,NA18502,NA18504,NA18505,NA18507,NA18508,NA18516,NA18517,NA18519,NA18520,NA18522,NA18523,NA18853,NA18856,NA18858,NA18861,NA18867,NA18868,NA18870,NA18871,NA18873,NA18874,NA18912,NA18916,NA18917,NA18933,NA18934 input_indexed.vcf.gz | bgzip -c > subset_output.vcf.gz
```

##### infos on variants
ALL.wgs.phase3_shapeit2_mvncall_integrated_v5b.20130502.sites.vcf.gz contains informations on each variant.
```
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO
1	10177	rs367896724	A	AC	100	PASS	AC=2130;AF=0.425319;AN=5008;NS=2504;DP=103152;EAS_AF=0.3363;AMR_AF=0.3602;AFR_AF=0.4909;EUR_AF=0.4056;SAS_AF=0.4949;AA=|||unknown(NO_COVERAGE);VT=INDEL
1	10235	rs540431307	T	TA	100	PASS	AC=6;AF=0.00119808;AN=5008;NS=2504;DP=78015;EAS_AF=0;AMR_AF=0.0014;AFR_AF=0;EUR_AF=0;SAS_AF=0.0051;AA=|||unknown(NO_COVERAGE);VT=INDEL
1	10352	rs555500075	T	TA	100	PASS	AC=2191;AF=0.4375;AN=5008;NS=2504;DP=88915;EAS_AF=0.4306;AMR_AF=0.4107;AFR_AF=0.4788;EUR_AF=0.4264;SAS_AF=0.4192;AA=|||unknown(NO_COVERAGE);VT=INDEL
1	10505	rs548419688	A	T	100	PASS	AC=1;AF=0.000199681;AN=5008;NS=2504;DP=9632;EAS_AF=0;AMR_AF=0;AFR_AF=0.0008;EUR_AF=0;SAS_AF=0;AA=.|||;VT=SNP
```


## Combining data for 48 individuals

I subsetted the VCF (by columns) to get only the individuals of interest and I now need to create VCF files with intersected variants for both types of data (i.e. for the 40 and 8 individuals datasets). I obtained one intersected file per dataset and per chromosome:
```
affy.chr10_selected_intervar.vcf.gz  ALL.chr10_selected_intervar.vcf.gz
affy.chr11_selected_intervar.vcf.gz  ALL.chr11_selected_intervar.vcf.gz
affy.chr12_selected_intervar.vcf.gz  ALL.chr12_selected_intervar.vcf.gz
affy.chr13_selected_intervar.vcf.gz  ALL.chr13_selected_intervar.vcf.gz
affy.chr14_selected_intervar.vcf.gz  ALL.chr14_selected_intervar.vcf.gz
affy.chr15_selected_intervar.vcf.gz  ALL.chr15_selected_intervar.vcf.gz
affy.chr16_selected_intervar.vcf.gz  ALL.chr16_selected_intervar.vcf.gz
affy.chr17_selected_intervar.vcf.gz  ALL.chr17_selected_intervar.vcf.gz
affy.chr18_selected_intervar.vcf.gz  ALL.chr18_selected_intervar.vcf.gz
affy.chr19_selected_intervar.vcf.gz  ALL.chr19_selected_intervar.vcf.gz
affy.chr1_selected_intervar.vcf.gz   ALL.chr1_selected_intervar.vcf.gz
affy.chr20_selected_intervar.vcf.gz  ALL.chr20_selected_intervar.vcf.gz
affy.chr21_selected_intervar.vcf.gz  ALL.chr21_selected_intervar.vcf.gz
affy.chr22_selected_intervar.vcf.gz  ALL.chr22_selected_intervar.vcf.gz
affy.chr2_selected_intervar.vcf.gz   ALL.chr2_selected_intervar.vcf.gz
affy.chr3_selected_intervar.vcf.gz   ALL.chr3_selected_intervar.vcf.gz
affy.chr4_selected_intervar.vcf.gz   ALL.chr4_selected_intervar.vcf.gz
affy.chr5_selected_intervar.vcf.gz   ALL.chr5_selected_intervar.vcf.gz
affy.chr6_selected_intervar.vcf.gz   ALL.chr6_selected_intervar.vcf.gz
affy.chr7_selected_intervar.vcf.gz   ALL.chr7_selected_intervar.vcf.gz
affy.chr8_selected_intervar.vcf.gz   ALL.chr8_selected_intervar.vcf.gz
affy.chr9_selected_intervar.vcf.gz   ALL.chr9_selected_intervar.vcf.gz
```
which I need to combine afterwards:
```
chr1.vcf.gz
chr2.vcf.gz
chr3.vcf.gz
chr4.vcf.gz
chr5.vcf.gz
chr6.vcf.gz
chr7.vcf.gz
chr8.vcf.gz
chr9.vcf.gz
chr10.vcf.gz
chr11.vcf.gz
chr12.vcf.gz
chr13.vcf.gz
chr14.vcf.gz
chr15.vcf.gz
chr16.vcf.gz
chr17.vcf.gz
chr18.vcf.gz
chr19.vcf.gz
chr20.vcf.gz
chr21.vcf.gz
chr22.vcf.gz
```
These are my final vcf.

You first need to index your samples with tabix:
```
tabix -p vcf input_indexed.vcf.gz
```
You  then create the phase3 intersected file:
```
perl vcf-isec -f -n =2 ../../../../../../data/burny/Genotype1000Grelease20130502/40LCLs/ALL.chr1_selected.vcf.gz ../../../../../../data/burny/Genotype1000Grelease20130502/48LCLs/8LCLs/ALL.wgs.nhgri_coriell_affy_6.20140825.genotypes_has_ped_selected.vcf.gz | bgzip -c > ../../../../../../data/burny/Genotype1000Grelease20130502/48LCLs/grouping/ALL.chr1_selected_intervar.vcf.gz
```
and the affy intersected file in a similar way:
```
perl vcf-isec -f -n =2 ../../../../../../data/burny/Genotype1000Grelease20130502/48LCLs/8LCLs/ALL.wgs.nhgri_coriell_affy_6.20140825.genotypes_has_ped_selected.vcf.gz ../../../../../../data/burny/Genotype1000Grelease20130502/48LCLs/grouping/ALL.chr1_selected_intervar.vcf.gz | bgzip -c > ../../../../../../data/burny/Genotype1000Grelease20130502/48LCLs/grouping/affy.chr1_selected_intervar.vcf.gz
```
and I make the combination with:
```
perl vcf-merge ../../../../../../data/burny/Genotype1000Grelease20130502/48LCLs/grouping/ALL.chr1_selected_intervar.vcf.gz ../../../../../../data/burny/Genotype1000Grelease20130502/48LCLs/grouping/affy.chr1_selected_intervar.vcf.gz | bgzip -c > ../../../../../../data/burny/Genotype1000Grelease20130502/48LCLs/grouping/chr1.vcf.gz
```

I created a 2 column tab-separated panel file containing basic informations on our individuals
```
"sample" "pop"
"NA18486" "YRI"
"NA18488" "YRI"
"NA18489" "YRI"
```

I converted .vcf.gz files for PLINK input by generating .map and .ped extension files:
```
vcftools --gzvcf ../../../../../../data/burny/Genotype1000Grelease20130502/48LCLs/grouping/chr1.vcf.gz --plink --out  ../../../../../../data/burny/Genotype1000Grelease20130502/48LCLs/grouping/tmp_chr1
```
```
tmp_chr1.ped; tmp_chr1.map
tmp_chr2.ped; tmp_chr2.map
tmp_chr3.ped; tmp_chr3.map
```

To evict the fact that the previous ped produced when converting vcf to PLINK input is empty:
```
NA19098 NA19098 0       0       0       0
NA19099 NA19099 0       0       0       0
NA19107 NA19107 0       0       0       0
```
I replace the first 6 columns of each ped by informations of ordered individuals from ../../../../../../data/burny/Genotype1000Grelease20130502/40LCLs/integrated_call_samples_v2.20130502.ALL.ped: 
```
Y105 NA19098 0 0 1 0
Y105 NA19099 0 0 2 0
Y006 NA19107 0 0 1 0
```
Final ped files are:
```
chr1.ped
chr2.ped
chr3.ped
```
The main idea of the code is to cut -f7 tmp_chr.ped and paste the individuals informations in the first 6 columns.


