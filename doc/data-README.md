# Where is the data ?

- The data of single-cell expression and SNP genotypes is not in this repository but at the [Biostudies](https://www.ebi.ac.uk/biostudies) website of the European Bioinformatics Institute under accession number
> S-BSST382

- The NGS sequencing data (paired-end 150nt reads from Illumina MiSeq) for clonality analysis is not in this repository either. It is available from the [European Nucleotide Archive](https://www.ebi.ac.uk/ena/) under study accession number
> PRJEB37875

# Description of data files 

You must create, at the root of this project, a directory called `data/`. Then download the archive from Biostudies, place it in the `data/ directory and unzip it.
You will then obtain the folders and files described below.

## Raw flow cytometry data files

- `data/gerard_primary/` : contains the primary flow-cytometry data.
- `data/20170522/` : contains several csv files (tables) corresponding to the lists of samples and their correspondance with primary data files.

## Data files used for linkage

The following data is used by `src/plink.Rnw`:

1. `data/2017-02-22/` : contains information about the genes loci.
2. `data/2017-05-23/processedData/` (the .txt file is needed here) : contains phenotypic data of LCLs and informations about cell lines, date of immunostaining experiment etc...
3. Directories `data/2019-06-07/40LCLs/` and `data/2019-06-07/48LCLs/` contain numerous files with grouped information of phenotypes and genotypes for 40 and 48 Yoruba individuals, respectively. Note that, although the 40 individuals are included in the 48 ones, the genetic map differs between the two datasets (see methods section of the publication). Each of these two directory contains:

  - `LCLs_id.txt` : The list of LCLs considered (one per individuals, these are identifiers from the Coriell institute)
  - A series of `.map` and `.ped` files with names in the form `<PROTEIN>_chr<i>_deltaTSS2e+06_<TRAIT>.wp_nodup_filt_rlm_mean_corr_<N>_LCLs.*`. The `.map` file contains SNPs information, the `.ped` file contains genotypes and trait values. One row per individual; Genotypes printed as allelic sequences. Every file name is organized such that:
    - `<PROTEIN>` is one of CD23, CD55, CD63, CD86, for which the trait was computed.
    - `<i>` is the chromosome number of the gene coding for the <PROTEIN>
    - `<TRAIT>` is one of:
        - `mean.claire` : mean expression
        - `cv.claire`   : CV of expression
        - `cv.claire.loess` : expression dispersion (CV | mean)
        - `mean1` and `var1` (for CD23) : mean and variance of the first component of the GMM model, respectively.
        - `mean2` and `var2` (for CD23) : mean and variance of the second component of the GMM model, respectively.
        - `pro1` (for CD23): proportion of cells attributed to the first component of the GMM model.

Some information on the procedure by which Claire Burny downloaded the genotypes and formatted them is described [here](doc/how_genotypes_were_extracted.md)


## Data files used for (most) figures

Input data files are organised in `data/YYYY-MM-DD` directories. The indicated date YYYY-MM-DD corresponds to a specific compilation of the primary data, from one or several series of experiments. These dates are:
```
2017-04-27
2017-05-23
2017-06-26
```
These directories contain a `processedData` subdirectory, which has two files:
- A text file with a long name `annotation_newval_stainedDAPI_gated_G1_corr_rlm_mean_filt_wpml_without_extreme_loess.txt`. This file contains annotations about cell lines, the date of immunostaining experiment and summary statistics of the expression values.
- A compressed data file with a long name `newval_stainedDAPI_gated_G1_corr_rlm_mean_wp.RData`. This file contains the single-cell data. Once it is loaded in R using the readRDS() function, the corresponding data is available as a list of data.frames, each data.frame corresponding to one sample (one row per event).

In addition, directory `data/2017-05-23/` contains an subdirectory `FitData` which has a file
`fit_gmm_model_CD23_ordered_components_rlm_mean_without_extreme_pooled.txt` containing the results of the Gaussian Mixture Model fitted to the distributions of CD23 expression. If you ran the model fitting yourself using the src/gmm.Rnw script, you then need to update this path in subsequent steps (this redirection is not done automatically yet).

Finally, the text file `data/2018-02-15/sampledays_for_anno_series.txt` contains information about the acquisitions, with dates, immunostaining and the 'series' to which the experiment belongs.

## Data files corresponding to the fixation/no-fixation control experiment

This data is located in two directories:
```
data/2019-12-18
data/2019-12-26
```
Each directory has an annotation file that describes the samples.

