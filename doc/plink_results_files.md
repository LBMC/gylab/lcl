# Where to find your results after running plink.Rnw   

For a full documentation on PLINK usage and input/output files format, please see the [PLINK Documentation](http://zzz.bwh.harvard.edu/plink/index.shtml)

## Output directories

The following directories should have been created where you ran plink.Rnw:

1. `plink-intermediates/` : contains files corresponding to the input data passed to PLINK. This includes .bed, .bim, .fam, .log, .map and .ped files.
2. `plink-results-40LCLs/`: contains the linkage results obtained when using 40 LCLs for which a dense genetic map was available.
3. `plink-results-48LCLs/` : contains the linkage results obtained when using 48 LCLs. These 48 LCLs include the 40LCLs subset mentioned above, and the genetic map is less dense.
4. Each `plink-results-*LCLs/` directory then contains subdirectories named in the format `CDxx_chrx_xxx/` where:
    - CDxx is the cell surface protein considered (eg CD86)
    - chrx is the chromosome harbouring the gene coding for this protein(eg chr3)
    - xxx is the trait for which linkage was searched (eg cv). trait cv.loess is the CV | mean residual (which is the expression dispersion); mean1, mean2, var1, var2 and pro1 are the GMM best-fit parameters for CD23; and pooled.var is pooled variance of the two GMM components (for CD23).

## Output files format
Each linkage scan produces 6 files:
1. A `.txt` file containing the trait values for each individual.
2. A `.log` file containing informations on the computational run.
3. A `.qassoc` file containing linkage association results with the following fields:
     - CHR:   Chromosome number
     - SNP:   SNP identifier
     - BP:    Physical position (base-pair)
     - NMISS: Number of non-missing genotypes
     - BETA:  Regression coefficient
     - SE:    Standard error
     - R2:    Regression r-squared
     - T:     Wald test (based on t-distribution)
     - P:     Wald test asymptotic p-value
 
4. A `.qassoc.adjusted` file containing adjusted-significance estimates, with the following fields:
     - CHR:      Chromosome number
     - SNP:      SNP identifer
     - UNADJ:    Unadjusted p-value
     - GC:       Genomic-control corrected p-values
     - BONF:     Bonferroni single-step adjusted p-values
     - HOLM:     Holm (1979) step-down adjusted p-values
     - SIDAK_SS: Sidak single-step adjusted p-values
     - SIDAK_SD: Sidak step-down adjusted p-values
     - FDR_BH:   Benjamini & Hochberg (1995) step-up FDR control
     - FDR_BY:   Benjamini & Yekutieli (2001) step-up FDR control

5. A `.qassoc.means` file containing means and standard deviations fo the trait stratified by genotype. Format:

     - CHR:     Chromosome code
     - SNP:     SNP identifier
     - VALUE:   Description of next three fields
     - G11:     Value for first genotype
     - G12:     Value for second genotype
     - G22:     Value for third genotype

where VALUE is one of GENO, COUNTS, FREQ, MEAN or SD (standard deviation). For example:

 | CHR | SNP        | VALUE  | G11     | G12     |G22      |
 | --- | ---------- | -----  | ------- | ------- |-------- |
 | 12  | rs17102261 | GENO   |   G/G   |   G/T   |   T/T   |
 | 12  | rs17102261 | COUNTS |       8 |      22 |      18 |
 | 12  | rs17102261 | FREQ   | 0.1667  | 0.4583  |  0.375  |
 | 12  | rs17102261 | MEAN   | 0.08404 | 0.08424 | 0.08377 |
 | 12  | rs17102261 | SD     | 0.007839| 0.008189| 0.005553|
 | 12  | rs12227447 | GENO   |   T/T   |   T/C   |   C/C   |
 | 12  | rs12227447 | COUNTS |       8 |      22 |      18 |
 | 12  | rs12227447 | FREQ   | 0.1667  | 0.4583  |  0.375  |
 | 12  | rs12227447 | MEAN   | 0.08404 | 0.08424 | 0.08377 |
 | 12  | rs12227447 | SD     | 0.007839| 0.008189| 0.005553|

i.e. each SNP takes up 5 rows

6. A `.qassoc.mperm` file containing empirical p-values computed from permutation:
      - EMP1: nominal p-value
      - EMP2: p-value corrected for the number of SNPs tested.


