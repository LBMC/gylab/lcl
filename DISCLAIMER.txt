#######################################################
#
# DISCLAIMER
#
# This data and software are provided with
# ABSOLUTELY NO WARRANTY
#
# If you are using this data, software, documentation
# and any material contained in this repository
# you are doing so AT YOUR OWN RISK.
#
# The information contained in these files is provided FOR RESEARCH USE ONLY
# and the authors can in no way be held responsible for 
# any damage (financial, physical, medical, psychological or of any nature)
# resulting from the use of the content of these files.
#
#########################################################

