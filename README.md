# Analysis of flow-cytometry data of Lymphoblastoid Cell Lines (LCL) immunostained for cell-surface proteins.

This study focused on inter-individual variation of cell-to-cell heterogeneity (variability) in gene expression.

## Authors

Contributors of data: Gerard Triqueneaux, Orsolya Symmons
Contributors of code: Claire Burny, Gael Yvert
Design and supervision: Gael Yvert
Laboratory of Biology and Modeling of the Cell, CNRS, ENS de Lyon, 46 allée d'Italie, 69007 Lyon, France.

## What you will find here

This repository contains analysis code asscoiated with the publication:
> Triqueneaux et al. Cell-to-cell expression dispersion of B-cell surface proteins is linked to genetic variants in humans, Communications Biology 2020.

You will also find [here](doc/data-README.md) a description of the associated data.


## What you will **NOT** find here

**The DATA itself**:

The data is not in this repository but must be downloaded from other locations. Please read the [documentation](doc/data-README.md) describing the data and where you can find it.

## Analysis code organization

The analysis code is organized in several main files, which should be run one after the other, in the following order.

- (step0, optional) `src/clairegate.Rnw` : runs preprocessing of cytometry data, including gating steps.
- (step1, optional) `src/gmm.Rnw` : runs Gaussian Mixture Models on CD23 distributions (one model per sample).
- (step2) `src/plink.Rnw` : runs linkage analysis using plink.
- (step3) `src/figures.Rnw` : produces most figures of the paper.
- (step4) `src/gview.Rnw` : produces figures of a genomic locus with various information.
- (step5) `src/fixation-CD23.Rnw` and `src/fixation-CD86.Rnw`: analysis of the control experiment comparing immunolabelling of fixed vs. unfixed cells.

In addition, directory `clonality/` contains information and code related to the PCR-SEQ Clonality analysis of the publication. We provide, for information, a Sweave code `results/2015-07-10/clonality.Rnw` which uses routines written in subdirectory `src/20150701/`.

## Step 0 (optional), Preprocessing of flow cytometry data

Preprocessing involves gating steps that take time to run, and which must be run on all data of a consistent series. To avoid running it every time, we stored the resulting processed data in directory `data/`. You can omit pre-processing and proceed to the next steps.

If you want to run pre-processing, you can access the code in `src/clairegate.Rnw`. It is set to run only on three arbitrarily-chosen samples (1, 2 and 20). If you want to run it on other samples, open the code and look for the following line:
```
runOnSamples = c(1,2,20); # to run only on those samples, delete this line to run on all samples;
```
and change c(1,2,20) by your choice of samples, or delete the line to run on all samples.

##  Step 1 (optional), GMM model fitting for CD23 ###

The `src/gmm.Rnw` code computes GMM parameters for CD23 expression distributions. Note that it takes a substantial computation time and was written to be parallelized on multiple cores. To avoid running it every time, we stored the resulting fitted data in directory `data/`. You can omit this model-fitting step and proceed to the next steps.

If you want to run this step, go into a subdirectory of the results folder, for example:
```sh
cd ./results/2018-01-30/
```

Then copy the script in this subdirectory:
```sh
cp ../../src/gmm.Rnw .
```

And run it, preferentially in the background using nohup so that you can close your ssh connexion while it runs.
```sh
nohup ../../src/Sweave.sh -ld -nc gmm.Rnw &
```

The `gmm.pdf` file produced gives a record of your computation. You should see a novel directory `GMM_CD23/` which contains a tab-delimited file with trait values including the GMM parameters that were computed.

##  Step 2, linkage analysis ###

### Data for linkage analysis

The `src/plink.Rnw` code uses the data that were prepared to combine genotypes and phenotypes. Details regarding these data files are available in documentation [data_README.md](doc/data_README.md).

### Running linkage analysis

To run this step, go into a subdirectory of the results folder, for example:
```sh
cd ./results/2018-01-30/
```

Then copy the script in this subdirectory:
```sh
cp ../../src/plink.Rnw .
```

And run it:
```sh
../../src/Sweave.sh -ld -nc plink.Rnw
```

See the `plink.pdf` file produced. Please see the dedicated [documentation](doc/plink_results_files.md) describing the organization and format of the plink output files containing the linkage results.

## Step 3, figures of the gene expression data

### Data used to generate figures

The `src/figures.Rnw` code uses several input files. For details, please see the documentation provided in [data_README.md](doc/data_README.md).


### Producing figures

To run this step, go into a subdirectory of the results folder, where you previoulsy ran `plink.Rnw`, for example:
```sh
cd ./results/2018-01-30/
```

Then copy the script in this subdirectory:
```sh
cp ../../src/figures.Rnw .
```

And run it:
```sh
../../src/Sweave.sh -ld -nc figures.Rnw
```

### Output figures and tables files

- `figures.pdf` : gives a general compilation of the run, with most figures.
- local directory `Figures/` contains all figures in PDF format. Exact fig numbers may have changed in the final version of the article.
- local directory `Tables/` contains tables provided in the supplementary material of the publication.

## Step 4, plots of genomic loci

### Producing the plot

To run this step, go into a subdirectory of the results folder, where you previoulsy ran `plink.Rnw`, for example:
```sh
cd ./results/2018-01-30/
```

Then copy the script in this subdirectory:
```sh
cp ../../src/gview.Rnw .
```

And run it:
```sh
../../src/Sweave.sh -ld -nc gview.Rnw
```

We sometimes (often) experienced halted execution with the error message

>Error in useDataset(mart = mart, dataset = dataset, verbose = verbose) : 
>  The given dataset:  hsapiens_gene_ensembl , is not valid. 

This is likely a connection issue with bioMart and we noticed that it is solved by running the command again:

```sh
../../src/Sweave.sh -ld -nc gview.Rnw
```

See the `gview.pdf` file produced.

## Step 5, control experiment on fixation

Similarly to the steps above, run:

```sh
cd ./results/2018-01-30/
cp ../../src/fixation-CD23.Rnw .
cp ../../src/fixation-CD86.Rnw .
../../src/Sweave.sh -ld -nc fixation-CD23.Rnw
../../src/Sweave.sh -ld -nc fixation-CD86.Rnw
```


### Relic ####
Another script is present that will likely NOT run on your instance:
src/kanto_prep_input.Rnw
This is a relic of a temporary code that Claire B. used to re-format the data in a way that can be exploited by Stephane J. to apply PTLMAPPER.
