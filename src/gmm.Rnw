\documentclass[a4paper]{article}
\usepackage{pdfpages}
\usepackage[latin9]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{color}
\usepackage{Sweave}
\usepackage{pdfcolmk}
\usepackage{ifthen}
\usepackage{multido}
\usepackage[labelformat = empty]{caption}
\usepackage{fancyhdr}
\usepackage[top=1.2cm, bottom=0.5cm, right=1.3cm, left=1.3cm]{geometry}
\usepackage{lastpage}
\pagestyle{fancy}
\usepackage{fancyvrb}
\usepackage{pdfpages}
\usepackage{etoolbox}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true, %set true if you want colored links
    linktoc=all,     %set to all if you want both sections and subsections linked
    linkcolor=blue,  %choose some color if you want links to stand out
}
\makeatletter
\patchcmd{\FV@ListVSpace}{\@topsepadd\topsep}{}{}  %% Reduce space between Sin/output
\makeatother
\fancyhf{}
\rhead{Version 20170508 - \date\today \hspace{0.2cm}- \thepage/\pageref{LastPage}}
\renewcommand{\footrulewidth}{0pt}
\newcommand{\bslash}{\textbackslash}
\nopagebreak

\begin{document}

\setlength\parindent{0pt}
\SweaveOpts{tidy = FALSE, keep.source = TRUE, keep.space = FALSE, keep.blank.space = FALSE, keep.comment = TRUE}
\DefineVerbatimEnvironment{Sinput}{Verbatim}{formatcom = {\color[rgb]{0, 0, 0.56}}}
\DefineVerbatimEnvironment{Soutput}{Verbatim}{formatcom = {\color[rgb]{0.56, 0, 0}}}

<< options, echo = FALSE , results = hide>>=
options(prompt = ">", continue = "...", width = 100)
@

\title{GMM model fitting to CD23 distributions}
\author{Claire Burny, Gerard Triqueneaux, Gael Yvert}
\maketitle
\tableofcontents
\newpage

\section{Settings and sourcing useful functions}

When and where we are :
<<dir-and-date, echo = FALSE>>=
### cleanup ###
rm(list=ls())

### When and where we are #####
date();
setwd(".")
getwd();

### create subdirectories #####
for (subdir in c("GMM_CD23/")){ 
  if (! (subdir %in% dir()) ) system(paste("mkdir -p ", subdir, sep = " "));
};

##### Load Packages #####
require(mclust)
library(parallel)

### Report on the status of this session ####
print(sessionInfo())
@

This code derives from the initial code of Claire Burny who generated the GMM parameters fitted to CD23 distributions.
Copied from /home/burny/LCL/results/20170523/lcl.Rnw .  It was written here by Gael Yvert to ensure that we can reproduce the generation of parameter values.

\section { Path to data and load it }

<<>>=
dir.proc <- "../../data/2017-05-23/processedData/";

mat.end.wp <- readRDS(paste(dir.proc, "newval_stainedDAPI_gated_G1_corr_rlm_mean_wp.RData", sep = ""))

info <- read.table(paste(dir.proc, "annotation_newval_stainedDAPI_gated_G1_corr_rlm_mean_filt_wpml_without_extreme_loess.txt", sep = ""), sep = "\t", h = T)

dir.fit  = "./GMM_CD23/";
file.fit = "fit_gmm_model_CD23_ordered_components_rlm_mean_without_extreme.txt";
@


\section { GMM fitting }

<<gmm, echo = FALSE, results = hide, eval = TRUE>>=
outfile = paste(dir.fit, file.fit, sep = "")

if (!file.exists(outfile)){
   # Fit GMM models and extract parameters to consider them as traits.
   id.CD23 <- info[which(info$marker == "CD23"), ]$number;
   nb.CD23 <- length(id.CD23);

   names <- c("BIC1", "BIC2E", "BIC2V", "var1", "var2", "var1.se", "var2.se", "mean1", "mean2", "mean1.se", "mean2.se", "pro1", "pro2", "pro1.se", "pro2.se");
  namesbis <- c("BIC1", "BIC2E", "BIC2V", "var2", "var1", "var2.se", "var1.se", "mean2", "mean1", "mean2.se", "mean1.se", "pro2", "pro1", "pro2.se", "pro1.se")

   # defines GMM function
   gmm <- function(x){
      require(mclust)
      tmp.id <- id.CD23[x];
      tmp.res <- c(tmp.id);
      tmp.res <- c(tmp.res, rep(NA, times = 15)); # for compatibility with old code from Claire, although here we don't treat cases where suff = ""
      suff = ".wp"; # for compatibility with old code from Claire
      {
   	newdata.df <- mat.end.wp[[as.character(tmp.id)]][, paste("log.fluo.corr", suff, sep = "")];
	n <- length(newdata.df);
	mod  <- Mclust(newdata.df, modelNames = "V", G = 2); BIC2V = -2*mod$loglik+log(n)*mod$df; # could call mod$BIC instead
	modE <- Mclust(newdata.df, modelNames = "E", G = 2); BIC2E = -2*modE$loglik+log(n)*modE$df;
	mod1 <- Mclust(newdata.df, modelNames = "E", G = 1); BIC1 = -2*mod1$loglik+log(n)*mod1$df;
	boot <- MclustBootstrap(mod, nboot = 1000, type = "bs");
	summary.boot.se <- summary(boot, what = "se");
	#summary.boot.ci <- summary(boot, what = "ci")

	param <- mod$parameters;
	pro <- param$pro; pro.se <- summary.boot.se$pro;
	mean <- param$mean; mean.se <- summary.boot.se$mean[1,];
	var <- param$variance$sigmasq; var.se <- summary.boot.se$variance[1,,];
	tmp.res <- c(tmp.res, c(BIC1,BIC2E,BIC2V,var,var.se,mean,mean.se,pro,pro.se))
      };
      tmp.res <- as.data.frame(matrix(tmp.res, nrow = 1, dimnames = list(NULL, c("number", names, paste(names, ".wp", sep = "")))));
	#if (tmp.res$mean1>tmp.res$mean2) {colnames(tmp.res) <- c("number", namesbis, paste(names, ".wp", sep = ""))};
      tmp.names <- colnames(tmp.res)
      if (tmp.res$mean1.wp>tmp.res$mean2.wp) {
         colnames(tmp.res) <- c(tmp.names[1:(length(names)+1)], paste(namesbis, ".wp", sep = ""))
      }
      return(tmp.res);
   }

   # run it (parallelized)
   no_cores <- detectCores()-1;
   cl <- makeCluster(no_cores);
   clusterExport(cl, c("id.CD23", "mat.end.wp", "names", "namesbis"));
   l <- parLapply(cl, seq_along(id.CD23), fun = gmm)
   #l <- parLapply(cl, 4:5, fun = gmm)
   stopCluster(cl)

   # extract outputs
   res.tmp <- do.call(rbind, l)
   info.tmp <- info[sapply(res.tmp$number, function(x) which(info$number == x)), ]
		
   annot.CD23 <- cbind(info.tmp, res.tmp[, -1])
   annot.CD23$abs.delta.mean    <- abs(annot.CD23$mean1-annot.CD23$mean2)
   annot.CD23$abs.delta.pro     <- abs(annot.CD23$pro1-annot.CD23$pro2)
   annot.CD23$abs.delta.var     <- abs(annot.CD23$var1-annot.CD23$var2)
   annot.CD23$delta.mean        <- annot.CD23$mean2-annot.CD23$mean1
   annot.CD23$delta.pro         <- annot.CD23$pro2-annot.CD23$pro1
   annot.CD23$delta.var         <- annot.CD23$var2-annot.CD23$var1
   annot.CD23$abs.delta.mean.wp <- abs(annot.CD23$mean1.wp-annot.CD23$mean2.wp)
   annot.CD23$abs.delta.pro.wp  <- abs(annot.CD23$pro1.wp-annot.CD23$pro2.wp)
   annot.CD23$abs.delta.var.wp  <- abs(annot.CD23$var1.wp-annot.CD23$var2.wp)
   annot.CD23$delta.mean.wp     <- annot.CD23$mean2.wp-annot.CD23$mean1.wp
   annot.CD23$delta.pro.wp      <- annot.CD23$pro2.wp-annot.CD23$pro1.wp
   annot.CD23$delta.var.wp      <- annot.CD23$var2.wp-annot.CD23$var1.wp

   data.gmm <- annot.CD23[which(annot.CD23$number %in% info$number), ]
   write.table(data.gmm, outfile, sep = "\t", col.names = T, row.names = F, quote = F)
   } else {
   print(paste("file", outfile, "already exists. Loading GMM traits from it."));
   data.gmm <- read.table(outfile, sep = "\t", header = TRUE)
}
@

<<>>=
date();
@

\end{document}
