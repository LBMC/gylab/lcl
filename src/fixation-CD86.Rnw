\documentclass[a4paper]{article}
\usepackage{pdfpages}
\usepackage[latin9]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{color}
\usepackage{Sweave}
\usepackage{pdfcolmk}
\usepackage{ifthen}
\usepackage{multido}
\usepackage[labelformat = empty]{caption}
\usepackage{fancyhdr}
\usepackage[top=1.2cm, bottom=0.5cm, right=1.3cm, left=1.3cm]{geometry}
\usepackage{lastpage}
\pagestyle{fancy}
\usepackage{fancyvrb}
\usepackage{pdfpages}
\usepackage{etoolbox}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true, %set true if you want colored links
    linktoc=all,     %set to all if you want both sections and subsections linked
    linkcolor=blue,  %choose some color if you want links to stand out
}
\makeatletter
\patchcmd{\FV@ListVSpace}{\@topsepadd\topsep}{}{}  %% Reduce space between Sin/output
\makeatother
\fancyhf{}
\rhead{Version 20170508 - \date\today \hspace{0.2cm}- \thepage/\pageref{LastPage}}
\renewcommand{\footrulewidth}{0pt}
\newcommand{\bslash}{\textbackslash}
\nopagebreak

\begin{document}

\setlength\parindent{0pt}
\SweaveOpts{tidy = FALSE, keep.source = TRUE, keep.space = FALSE, keep.blank.space = FALSE, keep.comment = TRUE}
\DefineVerbatimEnvironment{Sinput}{Verbatim}{formatcom = {\color[rgb]{0, 0, 0.56}}}
\DefineVerbatimEnvironment{Soutput}{Verbatim}{formatcom = {\color[rgb]{0.56, 0, 0}}}

<< options, echo = FALSE , results = hide>>=
options(prompt = ">", continue = "...", width = 100)
@

\title{Analysis of LCL data with of without PFA fixation prior to immunolabelling}
\author{Gerard Triqueneaux, Gael Yvert}
\maketitle
\tableofcontents
\newpage

\section{Settings and sourcing useful functions}

When and where we are :
<<dir-and-date, echo = FALSE>>=
### cleanup ###
rm(list=ls())

### When and where we are #####
date();
setwd(".")
getwd();

### create subdirectories #####
for (subdir in c("tmp/")){ 
  if (! (subdir %in% dir()) ) system(paste("mkdir -p ", subdir, sep = " "));
};

##### Load Packages #####
require(xtable)
library(flowCore)
library(flowViz)
library(flowStats)

require(ggplot2)

source("../../src/get.ith.elements.of.list.of.vectors.R")

### Report on the status of this session ####
print(sessionInfo())

@

\section { Load Data }

<<>>=
datarootdir = "../../data/"

######################
#        CD86        #
######################

# where
datasubdir = "2019-12-18/"
annofile = paste(datarootdir, datasubdir, "anno20191218-CD86.csv", sep = "")
datadir  = paste(datarootdir, datasubdir, sep = "")
anno = read.table(annofile, h = T, sep = ",", skip = 0, stringsAsFactors = F)

# initialize
data = list()
data$alldat = list()
data$anno = anno

# read data
n = dim(anno)[1]
i = 1
for (i in 1:n){
  file = paste(datadir, anno$filename[i], sep = "")
  data$alldat[[i]] = read.FCS(file, alter.names = T, transformation = F)
}
fs = flowSet(data$alldat)
@


\section{ Figures }

\subsection{ FSC-SSC plots to distinguish dead cells }

<<fig = TRUE>>=
i = 1
title = paste("Frame", i)
x = data$alldat[[i]]
flowPlot(x, plotParameters = c("FSC.A", "SSC.A"), logx = FALSE, logy = FALSE, main = paste(title, "All events"))
@

<<fig = TRUE>>=
xyplot(`SSC.A` ~ `FSC.A`, fs[1:9])
@


\subsection{ Gating }

\subsubsection{ Exclude 'debris' based on morphology }

We want to exclude 'debris' having small FSC and high SSC values.
FlowStats allows to build filters (data-driven gates) based on high-density regions. We use it and build a routine to automatically select the area of high FSC and low SSC value, corresponding to 'live' cells and not debris.

Compute filters:
<<computeFSCSSCFilters>>=
filename = "./tmp/fixation-filters-CD86.rda"
c2f <- curv2Filter("FSC.A", "SSC.A", bwFac=1.8)
if (!file.exists(filename)){
   print("computing FSC/SSC fiters. Please wait...")
   fr <- filter(fs, c2f)
   save(fr, file = filename)
   print("... [DONE]")
} else {
   print(paste("loading FSC/SSC fiters from file", filename))
   load(filename);
}
@


<<fig=TRUE>>=
i = 1:9
xyplot(`SSC.A` ~ `FSC.A`, data = fs[i], filter = fr[i])
@

Select area of interest and use it for gating:

<<>>=
nondeb.area <- function(x, ff, maxMedianSSC.A = 125000, min.events = 1000){
  # x: a filterResult
  # ff: the flowFrame from which the filterResult x was obtained
  
  # split data per filter areas
  sx = split(ff, x)

  # compute median FSC/SSC for each subgroup of events
  medians = list()
  medians[[1]] = NA
  for (i in 2:length(sx)) { # loop through areas
    # analyse data of this area 
    d = exprs(sx[[i]])
    if (dim(d)[1] > 1){ # at least 2 events fall in the area
       j = which(colnames(d) %in% c("FSC.A", "SSC.A"))
       medians[[i]] = apply(d[,j], MARGIN = 2, FUN = median)
    } else medians[[i]] = NA
  }
  SSCmed = get.ith.elements.of.list.of.vectors(medians,2)
  FSCmed = get.ith.elements.of.list.of.vectors(medians,1)

  # discard areas with extremely high median SSC
  SSC.keep = SSCmed < maxMedianSSC.A 

  # Of the remaining areas, select the one with highest medianFSC
  FSC.max = max(FSCmed[SSC.keep], na.rm = TRUE)
  area.index = which( SSC.keep & (FSCmed == FSC.max))

  # keep if contains enough events
  if (summary(x)$true[area.index] < min.events) area.index = NULL 
  
  return(area.index)
}

# for each frame, Subset on the gate defined by the selected area of non-debris
# store the gated data in a novel flowSet
gfs = list()
emptydata = exprs(fs[[1]])[1:2,]
emptydata[1:2,] <- NA
emptyFF = flowFrame(emptydata) 
for (i in 1:length(fr)){
   # get area gate
   a  = nondeb.area(fr[[i]], fs[[i]])
   # apply it
   sx = split(fs[[i]], fr[[i]])
   if (!is.null(a)) gfs[[i]] = sx[[a]]
   else gfs[[i]] = emptyFF 
}
gfs = flowSet(gfs)
@


<<fig=TRUE>>=
i = 1:9
xyplot(`SSC.A` ~ `FSC.A`, data = gfs[i], filter = fr[i])
@

\subsubsection{ Discard samples having only debris }

We see that for a few frames (samples) where only debris were present, the gating procedure selected... ...debris. We must discard these samples from further analysis. We can easily identifiy these samples by computing the post-gating median SSC (which is very high) and FSC value (which is very low).

<<>>=
medSSC <- function(x){
  # x: a flowFrame
  s = summary(x)
  i = which(rownames(s) == "Median")
  j = which(colnames(s) == "SSC.A")
  return(s[i,j])
}

medFSC <- function(x){
  # x: a flowFrame
  s = summary(x)
  i = which(rownames(s) == "Median")
  j = which(colnames(s) == "FSC.A")
  return(s[i,j])
}

SSCscores = fsApply(gfs, FUN = medSSC)
FSCscores = fsApply(gfs, FUN = medFSC)
@

<<fig=TRUE>>=
plot(SSCscores ~ FSCscores, xlab = "median FSC.A", ylab = "median SSC.A"); 
SSCcut = 100000
FSCcut = 40000
#rect(xleft = FSCcut, ybottom = 0, xright = Inf, ytop = SSCcut, lty = 2, border = 2)
abline(v = FSCcut, col = "red", lty = 2)
abline(h = SSCcut, col = "red", lty = 2)
@

We therefore exclude samples where median SSC.A value of gated cells is higher than \Sexpr{SSCcut} or median FSC.A value is lower than \Sexpr{FSCcut}

<<>>=
idx = which(SSCscores > SSCcut | FSCscores < FSCcut)
for (i in idx) gfs[[i]] <- emptyFF;
@

<<fig=TRUE>>=
i = 1:9
xyplot(`SSC.A` ~ `FSC.A`, data = gfs[i], filter = fr[i])
@


\subsection{ no trace of doublets }

Doublets correpsond to high FSC-H value and similar FSC-A value. We see on the following plots that there is no such things (or rare enough).

<<fig=TRUE>>=
par(mfcol = c(4,4))
for (i in 1:16){
  if (identical(summary(gfs[[i]]), summary(emptyFF))) plot(0,0, main = "Discarded")
  else flowPlot(gfs[[i]], plotParameters = c("FSC.H", "FSC.A"), logx = FALSE, logy = FALSE, main = paste("Frame", i))
}
@
<<fig=TRUE>>=
par(mfcol = c(4,4))
for (i in 17:32){
  if (identical(summary(gfs[[i]]), summary(emptyFF))) plot(0,0, main = "Discarded")
  else flowPlot(gfs[[i]], plotParameters = c("FSC.H", "FSC.A"), logx = FALSE, logy = FALSE, main = paste("Frame", i))
}
@
<<fig=TRUE>>=
par(mfcol = c(4,4))
for (i in 33:48){
  if (identical(summary(gfs[[i]]), summary(emptyFF))) plot(0,0, main = "Discarded")
  else flowPlot(gfs[[i]], plotParameters = c("FSC.H", "FSC.A"), logx = FALSE, logy = FALSE, main = paste("Frame", i))
}
@
<<fig=TRUE>>=
par(mfcol = c(4,4))
for (i in 49:64){
  if (identical(summary(gfs[[i]]), summary(emptyFF))) plot(0,0, main = "Discarded")
  else flowPlot(gfs[[i]], plotParameters = c("FSC.H", "FSC.A"), logx = FALSE, logy = FALSE, main = paste("Frame", i))
}
@
<<fig=TRUE>>=
par(mfcol = c(4,4))
for (i in 65:80){
  if (identical(summary(gfs[[i]]), summary(emptyFF))) plot(0,0, main = "Discarded")
  else flowPlot(gfs[[i]], plotParameters = c("FSC.H", "FSC.A"), logx = FALSE, logy = FALSE, main = paste("Frame", i))
}
@

\subsection { DAPI staining }

<<fig=TRUE>>=
# identify empty samples
n = length(gfs)
empty = rep(NA, times = n)
for (i in 1:n)
  empty[i] = identical(summary(gfs[[i]]), summary(emptyFF))
keep = which(!empty)

# plot all others
densityplot(~ `Pacific.Blue.A`, gfs[keep], xlim = c(-10000, 50000))
@


\section{ Fixation vs. No fixation }

<<>>=
plotone <- function(ff, logval = TRUE, ...){
    x = exprs(ff)
    j = which(colnames(x) == "PE.A")
    x = x[,j]
    x[x <= 0] <- NA
    if (logval) { 
       x = log(x)
       xlab = "log(PE.A)"
    } else xlab = "PE.A"
    x = na.omit(x)
    n = length(x) 
    if (n <= 10) {
        plot(0,0,...)
        legend("topleft", legend = paste(n, "cells"), lty = 1, col = 1, bty = "n")
    } else {
        d = density(x)
        plot(d, xlab = xlab, ...)
        legend("topleft", legend = paste(n, "cells"), lty = 1, col = 1, bty = "n")
    }
}

#init graph
xlim = c(0,12)

# per cell-line analysis.
x = data$anno
x$fixation <- as.character(x$fixation)
lclID = substr(data$anno$Ids,1, 4)
lcl = unique(lclID)

plotthisline <-function(l) {
   par(mfcol = c(4,2))

   # at least one fixed and one unfixed sample?
   f  <- (lclID == l) & (x$fixation == "yes")
   nf <- (lclID == l) & (x$fixation == "no")
   if (sum(f) > 0 & sum(nf) > 0) {
       for (i in which(f)) plotone(gfs[[i]], xlim = xlim, main = x$Ids[i])
       for (i in which(nf)) plotone(gfs[[i]], xlim = xlim, main = x$Ids[i])
   }
}
@


<<fig=TRUE>>=
plotthisline(lcl[1])
@
<<fig=TRUE>>=
plotthisline(lcl[2])
@
<<fig=TRUE>>=
plotthisline(lcl[3])
@
<<fig=TRUE>>=
plotthisline(lcl[4])
@
<<fig=TRUE>>=
plotthisline(lcl[5])
@
<<fig=TRUE>>=
plotthisline(lcl[6])
@
<<fig=TRUE>>=
plotthisline(lcl[7])
@
<<fig=TRUE>>=
plotthisline(lcl[8])
@
<<fig=TRUE>>=
plotthisline(lcl[9])
@
<<fig=TRUE>>=
plotthisline(lcl[10])
@

\end{document}
